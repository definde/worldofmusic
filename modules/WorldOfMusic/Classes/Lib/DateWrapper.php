<?php

namespace Dba\WorldOfMusic\Lib;

/**
 * Just a simple wrapper to hide the date time object....
 * @package Dba\WorldOfMusic\Lib
 */
class DateWrapper
{
    protected $dateObject;

    /**
     * @return \DateTime
     */
    public function getDateObject()
    {
        return $this->dateObject;
    }

    /**
     * @param mixed $dateObject
     */
    public function setDateObject(\DateTime $dateObject)
    {
        $this->dateObject = $dateObject;
    }


    public function __call($name, $arguments){
        if(count($arguments) > 0){
            $arguments = implode(',',$arguments);
            return $this->getDateObject()->$name($arguments);
        } else {
            return $this->getDateObject()->$name();
        }


    }
}