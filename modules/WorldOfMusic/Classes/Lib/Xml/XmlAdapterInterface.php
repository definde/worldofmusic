<?php

namespace Dba\WorldOfMusic\Lib\Xml;

/**
 * Interface XmlAdapterInterface
 * @package Dba\WorldOfMusic\Lib\Xml
 */
interface XmlAdapterInterface {

    public function addChild($name, $value = null, $namespace = null);
    public function asXml($filename);

}