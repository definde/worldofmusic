<?php

namespace Dba\WorldOfMusic\Lib\Xml;
use Dba\AwesomeMvc\Mvc\Service\ConfigurationService;
use Dba\WorldOfMusic\Domain\Model\Entity\Album;

/**
 * A very simple and incomplete wrapper for the php internal  simplexmlelement class.
 *
 * @package Dba\WorldOfMusic\Lib\Xml
 */
/**
 * Class SimpleXmlAdapter
 * @package Dba\WorldOfMusic\Lib\Xml
 */
class SimpleXmlAdapter implements XmlAdapterInterface
{

    /**
     * @var \SimpleXMLElement;
     */
    protected $simpleXml;

    /**
     * SimpleXmlAdapter constructor.
     */
    public function __construct()
    {
        //@todo well very crappy here...
        $this->setSimpleXml(new \SimpleXMLElement(
            "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><matchingReleases></matchingReleases>"
        ));
    }

    /**
     * @return mixed
     */
    public function getSimpleXml()
    {
        return $this->simpleXml;
    }

    /**
     * @param mixed $simpleXml
     */
    public function setSimpleXml($simpleXml)
    {
        $this->simpleXml = $simpleXml;
    }


    /**
     * Adds a child to the xml tree.
     * @param $name
     * @param null $value
     * @param null $namespace
     * @return mixed
     */
    public function addChild($name, $value = null, $namespace = null)
    {
        return $this->getSimpleXml()->addChild($name, $value, $namespace);
    }

    /**
     * Returns the xml stream as xml. If filename is given it creates a file in the filesystem.
     * @param $filename
     */
    public function asXml($filename = NULL)
    {
        $this->getSimpleXml()->asXml($filename);
    }


}