<?php

namespace Dba\WorldOfMusic\Lib\Xml;


/**
 * The xml Handler acts as a proxy to the concrete implementation of the defined adapterObject
 * .
 * @package Dba\WorldOfMusic\Lib\Xml
 */
class XmlHandler {

    /**
     * @var SimpleXmlAdapter
     */
    protected $adapter;

    public function __construct(SimpleXmlAdapter $adapter){
        $this->setAdapter($adapter);
    }
    /**
     * @return mixed
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * @param mixed $adapter
     */
    public function setAdapter(XmlAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }


    /**
     * Proxy to the concrete implementation.
     *
     * @param $name
     * @param null $value
     * @param null $namespace
     * @return mixed
     */
    public function addChild($name, $value = null, $namespace = null)
    {
        return $this->getAdapter()->addChild($name, $value, $namespace);
    }

    /**
     * Proxy to the concrete implementation.
     * @param $filename
     * @return mixed
     */
    public function asXml($filename)
    {
        return $this->getAdapter()->asXml($filename);
    }


}