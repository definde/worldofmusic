<?php

namespace Dba\WorldOfMusic\Domain\Model\Service;



use Dba\AwesomeMvc\Mvc\Service\ConfigurationService;
use Dba\WorldOfMusic\Domain\Model\Repository\AlbumRepository;
use Dba\WorldOfMusic\Lib\DateWrapper;

/**
 * A service class to organize queries against the album storage. Difference to the repository is that here might be
 * some more tasks that should be done to get a result and these tasks should happen in the service and not in the
 * repository.
 *
 * @package Dba\WorldOfMusic\Domain\Model\Service
 */
class AlbumQueryService extends AbstractQueryService  implements QueryServiceInterface {


    /**
     * AlbumQueryService constructor.
     * @param ConfigurationService $service
     * @param AlbumRepository $repositoryClass
     */
    public function __construct(ConfigurationService $service, AlbumRepository $repositoryClass){
        parent::__construct($service);
        $this->setRepository($repositoryClass);
    }

    /**
     * Proxies to the repository and returns all found entities with more than X tracks defined by the parameter.
     * @param int $numberOfResults
     * @return mixed
     */
    public function findWithMoreThenXTracksReleasedBefore($numberOfResults, DateWrapper $date){
        return $this->getRepository()->findWithMoreThenXTracksReleasedBefore($numberOfResults, $date);
    }
}