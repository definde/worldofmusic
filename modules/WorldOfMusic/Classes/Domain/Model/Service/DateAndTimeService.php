<?php

namespace Dba\WorldOfMusic\Domain\Model\Service;

use Dba\AwesomeMvc\Mvc\Service\BaseService;
use Dba\WorldOfMusic\Lib\DateWrapper;

class DateAndTimeService extends BaseService {

    public function createFromFormat($value, $format = 'd/m/Y'){
        $dateWrapper = $this->getConfigurationService()->getApp()->make(DateWrapper::class);
        $dateWrapper->setDateObject(\DateTime::createFromFormat($format, $value));
        return $dateWrapper;
    }
}