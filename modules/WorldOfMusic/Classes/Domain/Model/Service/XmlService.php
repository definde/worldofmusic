<?php

namespace Dba\WorldOfMusic\Domain\Model\Service;

use Dba\AwesomeMvc\Mvc\Service\BaseService;
use Dba\AwesomeMvc\Mvc\Service\ConfigurationService;
use Dba\AwesomeMvc\Mvc\Service\SanitizeService;
use Dba\WorldOfMusic\Domain\Model\Entity\Album;
use Dba\WorldOfMusic\Lib\Xml\XmlHandler;

/**
 * A service class to handle concrete business tasks on the XML domain.
 *
 * @package Dba\WorldOfMusic\Domain\Model\Service
 */
class XmlService extends BaseService
{

    /**
     * @var XmlHandler;
     */
    protected $xmlHandler;

    /**
     * @var SanitizeService;
     */
    protected $sanitizeService;

    /**
     * XmlService constructor.
     * @param ConfigurationService $service
     * @param XmlHandler $xmlHandler
     * @param SanitizeService $sanitizeService
     */
    public function __construct(ConfigurationService $service, XmlHandler $xmlHandler, SanitizeService $sanitizeService)
    {
        parent::__construct($service);
        $this->setXmlHandler($xmlHandler);
        $this->setSanitizeService($sanitizeService);
    }

    /**
     * @return mixed
     */
    public function getSanitizeService()
    {
        return $this->sanitizeService;
    }

    /**
     * @param mixed $sanitizeService
     */
    public function setSanitizeService(SanitizeService $sanitizeService)
    {
        $this->sanitizeService = $sanitizeService;
    }


    /**
     * @return mixed
     */
    public function getXmlHandler()
    {
        return $this->xmlHandler;
    }

    /**
     * @param mixed $xmlHandler
     */
    public function setXmlHandler(XmlHandler $xmlHandler)
    {
        $this->xmlHandler = $xmlHandler;
    }


    /**
     * Creates a xml list of all album entities that are given as parameter.
     * @param array $albums
     */
    public function createAlbumShortlistAsXml(array $albums, $xmlName)
    {
        $simpleXmlStream = $this->getXmlHandler();

        foreach ($albums as $album) {
            if($album instanceof Album){
                $releaseNode = $simpleXmlStream->addChild('release');
                $releaseNode->addChild('name', $this->getSanitizeService()->htmlSpecialChars($album->getName()));
                $releaseNode->addChild('trackCount', count($album->getTracks()));
            }
        }

        $simpleXmlStream->asXML($this->getConfigurationService()->getApp()->getDocumentRoot() . $xmlName . time() . '.xml');
    }

}

