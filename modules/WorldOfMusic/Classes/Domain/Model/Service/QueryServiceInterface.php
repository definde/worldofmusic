<?php

namespace Dba\WorldOfMusic\Domain\Model\Service;

use Dba\AwesomeMvc\Mvc\Repository\BaseRepository;

/**
 * Interface QueryServiceInterface
 * @package Dba\WorldOfMusic\Domain\Model\Service
 */
interface QueryServiceInterface {

    public function setRepository(BaseRepository $repository);
    public function getRepository();
}