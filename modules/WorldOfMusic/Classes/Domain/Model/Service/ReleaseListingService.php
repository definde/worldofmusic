<?php

namespace Dba\WorldOfMusic\Domain\Model\Service;

use Dba\AwesomeMvc\Mvc\Service\BaseService;
use Dba\AwesomeMvc\Mvc\Service\ConfigurationService;
use Dba\WorldOfMusic\Domain\Model\Entity\Album;

/**
 * A service class to handle tasks to create/delete/manipulate different listings of albums or/and releases.
 *
 * @package Dba\WorldOfMusic\Domain\Model\Service
 */
class ReleaseListingService extends BaseService {

    /**
     * @var AlbumQueryService;
     */
    protected $queryService;

    /**
     * @var XmlService;
     */
    protected $xmlService;

    public function __construct(ConfigurationService $service, XmlService $xmlService, AlbumQueryService $queryService)
    {
        parent::__construct($service);
        $this->setQueryService($queryService);
        $this->setXmlService($xmlService);
    }

    /**
     * @return mixed
     */
    public function getQueryService()
    {
        return $this->queryService;
    }

    /**
     * @param mixed $queryService
     */
    public function setQueryService(QueryServiceInterface $queryService)
    {
        $this->queryService = $queryService;
    }

    /**
     * @return mixed
     */
    public function getXmlService()
    {
        return $this->xmlService;
    }

    /**
     * @param mixed $xmlService
     */
    public function setXmlService(XmlService $xmlService)
    {
        $this->xmlService = $xmlService;
    }

    /**
     * Creates a xml list of all albums that are found which have more than x $numberOfTracks.
     * @param $numberOfTracks
     */
    public function createXmlWithMoreThanXTracksAndBeforeReleaseDate($numberOfTracks, $date){
        $albums = $this->getQueryService()->findWithMoreThenXTracksReleasedBefore($numberOfTracks, $date);
        $this->getXmlService()->createAlbumShortlistAsXml($albums, 'albums_with_more_than_'.$numberOfTracks.'_tracks_');
    }
}