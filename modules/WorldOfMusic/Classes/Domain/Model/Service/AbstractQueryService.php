<?php

namespace Dba\WorldOfMusic\Domain\Model\Service;

use Dba\AwesomeMvc\Mvc\Service\BaseService;
use Dba\AwesomeMvc\Mvc\Repository\BaseRepository;

/**
 * Abstract Query service class which should be used for concrete implementations of a queryservice.
 *
 * @package Dba\WorldOfMusic\Domain\Model\Service
 */
abstract class AbstractQueryService extends BaseService {


    /**
     *
     * @var
     */
    protected $repository;

    /**
     * @return mixed
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param mixed $repository
     */
    public function setRepository(BaseRepository $repository)
    {
        $this->repository = $repository;
    }




}