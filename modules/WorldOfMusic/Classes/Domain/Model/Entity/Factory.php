<?php

namespace Dba\WorldOfMusic\Domain\Model\Entity;

use Dba\AwesomeMvc\Mvc\Service\ConfigurationService;

/**
 * Factory to create entities by name.
 *
 * @package Dba\WorldOfMusic\Domain\Model\Entity
 */
class Factory {

    protected $configurationService;

    /**
     * @return mixed
     */
    public function getConfigurationService()
    {
        return $this->configurationService;
    }

    /**
     * @param mixed $configurationService
     */
    public function setConfigurationService($configurationService)
    {
        $this->configurationService = $configurationService;
    }



    protected $namespace = "Dba\\WorldOfMusic\\Domain\\Model\\Entity\\";
    public function __construct(ConfigurationService $configurationService)
    {
        $this->setConfigurationService($configurationService);

    }

    public function createEntity($entityName){
        $className = $this->namespace. $entityName;
        if(class_exists($className)){
            return $this->getConfigurationService()->getApp()->make($className);
        }
    }
}