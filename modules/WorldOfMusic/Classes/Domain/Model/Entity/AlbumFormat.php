<?php

namespace Dba\WorldOfMusic\Domain\Model\Entity;

/**
 * Representation of available formats a music album might exist (i.e. CD, LP, MP3)
 * @package Dba\WorldOfMusic\Domain\Model\Entity
 */
class AlbumFormat
{

    protected $id;
    protected $format;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param mixed $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }



}