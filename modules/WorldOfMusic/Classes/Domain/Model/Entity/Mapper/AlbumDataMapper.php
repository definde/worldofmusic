<?php

namespace Dba\WorldOfMusic\Domain\Model\Entity\Mapper;


use Dba\WorldOfMusic\Domain\Model\Entity\Mapper\MusicMozEntityMapper;
use Dba\AwesomeMvc\Persistence\DataMapper\BaseDataMapper;
use Dba\AwesomeMvc\Persistence\Storage\XmlStorageAdapter;

/**
 * Concrete implementation of a datamapper for the Album entity. Currently it uses the MusicMoz Entity Mapper in
 * combination with the xmlStorageAdapter.
 *
 * So if you would like to change the storage or the mapping of the entity you can easily inject different adapter.
 *
 *
 * @package Dba\WorldOfMusic\Domain\Model\Entity\Mapper
 */
class AlbumDataMapper extends BaseDataMapper
{

    /**
     * AlbumDataMapper constructor.
     * @param XmlStorageAdapter $storageAdapter
     * @param \Dba\WorldOfMusic\Domain\Model\Entity\Mapper\MusicMozEntityMapper $entityMapper
     */
    public function __construct(XmlStorageAdapter $storageAdapter, MusicMozEntityMapper $entityMapper)
    {
        parent::__construct($storageAdapter, $entityMapper);
    }

}