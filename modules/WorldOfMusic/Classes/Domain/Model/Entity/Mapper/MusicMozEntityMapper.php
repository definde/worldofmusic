<?php

namespace Dba\WorldOfMusic\Domain\Model\Entity\Mapper;


use Dba\WorldOfMusic\Domain\Model\Entity\Album;
use Dba\WorldOfMusic\Domain\Model\Entity\AlbumFormat;
use Dba\WorldOfMusic\Domain\Model\Entity\Factory;
use Dba\WorldOfMusic\Domain\Model\Entity\Track;
use Dba\AwesomeMvc\Persistence\DataMapper\EntityMapper\EntityMapperInterface;
use Dba\WorldOfMusic\Domain\Model\Service\DateAndTimeService;

/**
 * Concrete implementation of an entity mapper that maps from the MusicMoz xml structure to the internally used entity
 * setup.
 *
 * @package Dba\WorldOfMusic\Domain\Model\Entity\Mapper
 */
class MusicMozEntityMapper implements EntityMapperInterface {


    protected $entityFactory;

    protected $dateTimeService;

    public function __construct(Factory $entityFactory, DateAndTimeService $dateService){
        $this->setEntityFactory($entityFactory);
        $this->setDateTimeService($dateService);
    }

    /**
     * @return mixed
     */
    public function getDateTimeService()
    {
        return $this->dateTimeService;
    }

    /**
     * @param mixed $dateTimeService
     */
    public function setDateTimeService($dateTimeService)
    {
        $this->dateTimeService = $dateTimeService;
    }
    /**
     * @return mixed
     */
    public function getEntityFactory()
    {
        return $this->entityFactory;
    }

    /**
     * @param mixed $entityFactory
     */
    public function setEntityFactory($entityFactory)
    {
        $this->entityFactory = $entityFactory;
    }



    public function createEntityFromRow($row)
    {
        $entity = $this->getEntityFactory()->createEntity('Album');

        $entity->setTitle($row->title->__toString());
        $entity->setName($row->name->__toString());
        $entity->setGenre($row->genre->__toString());
        $entity->setReleaseDate($this->getDateTimeService()->createFromFormat($row->releasedate->__toString(), 'Y.m.d'));
        $entity->setLabel($row->label->__toString());

        foreach ($row->tracklisting->track as $track) {
            $trackEntity = new Track();
            $trackEntity->setTitle($track->__toString());
            $entity->addTrack($trackEntity);
        }

        $formats = explode(',', $row->formats->__toString());

        foreach ($formats as $format) {
            $formatEntity = new AlbumFormat();
            $formatEntity->setFormat($format);
            $entity->addFormat($formatEntity);
        }

        return $entity;
    }
}