<?php

namespace Dba\WorldOfMusic\Domain\Model\Entity;

/**
 * Representation of a single track. Can be part of an album of course.
 *
 * @package Dba\WorldOfMusic\Domain\Model\Entity
 */
class Track
{

    protected $id;

    protected $title;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


}