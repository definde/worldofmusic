<?php
namespace Dba\WorldOfMusic\Domain\Model\Repository;


use Dba\AwesomeMvc\Mvc\Repository\BaseRepository;
use Dba\WorldOfMusic\Lib\DateWrapper;


/**
 * The album repository is the gateway to the data layer where the data is stored.
 * @package Dba\WorldOfMusic\Domain\Model\Repository
 */
class AlbumRepository extends BaseRepository {


    /**
     *
     * @return mixed
     */
    public function findAll(){
        return $this->getDataMapper()->findAll();
    }

    public function findWithMoreThenXTracksReleasedBefore($numberOfTracks = 10, DateWrapper $date){
        $albums = $this->findAll();
        $result = array();

        foreach($albums as $album){
            if($album->getNumberOfTracks() > $numberOfTracks &&
                $album->getReleaseDate()->getTimeStamp() < $date->getTimestamp()){
                $result[] = $album;
            }
        }
        return $result;
    }
}