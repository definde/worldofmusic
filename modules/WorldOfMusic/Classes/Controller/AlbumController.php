<?php

namespace Dba\WorldOfMusic\Controller;

use Dba\AwesomeMvc\Mvc\Controller\BaseController;
use Dba\WorldOfMusic\Domain\Model\Service\ReleaseListingService;
use Dba\WorldOfMusic\Domain\Model\Service\DateAndTimeService;

/**
 * Controller that runs actions with the scope to the album domain within the worldofmusic module.
 *
 * @package Dba\WorldOfMusic\Controller
 */
class AlbumController extends BaseController {


    /**
     * This action fetches all albums from the storage with more tracks then defined in the numberOfTracks request
     * parameter. Default value is 10
     *
     * Based on the albums that are found a xml of them is being generated and stored.
     *
     * $releaseDate must be in the format "01/01/2001" d-m-Y
     * 
     * @param $releaseDate
     * @param $moreTracksThan
     */
    public function listByMoreThenXTracksAndReleasedBeforeDateAction(){
        $moreThanTracks = 10;
        $releaseBefore = "01/01/2001";

        $listingService = $this->get(ReleaseListingService::class);

        $moreThanTracksFromRequest = $this->getRequest()->getParam('moreTracksThan') ;
        if($moreThanTracksFromRequest && ctype_digit($moreThanTracksFromRequest)){
            $moreThanTracks = $moreThanTracksFromRequest;
        }


        $releaseBeforeFromRequest = $this->getRequest()->getParam('releaseBefore') ;

        if($releaseBeforeFromRequest){
            $releaseBefore = $releaseBeforeFromRequest;
        }

        $releaseDate = $this->get(DateAndTimeService::class)->createFromFormat($releaseBefore);
        $listingService->createXmlWithMoreThanXTracksAndBeforeReleaseDate($moreThanTracks, $releaseDate);
    }

}