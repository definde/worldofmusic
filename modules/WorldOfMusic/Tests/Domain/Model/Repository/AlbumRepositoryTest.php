<?php

namespace Dba\WorldOfMusic\Tests\Domain\Model\Repository;


use Dba\WorldOfMusic\Domain\Model\Entity\Album;
use Dba\WorldOfMusic\Domain\Model\Entity\Mapper\AlbumDataMapper;
use Dba\WorldOfMusic\Domain\Model\Entity\Track;
use Dba\WorldOfMusic\Domain\Model\Repository\AlbumRepository;
use Dba\WorldOfMusic\Lib\DateWrapper;
use PHPUnit\Framework\TestCase;

class AlbumRepositoryTest extends TestCase
{

    public function testFindAll()
    {
        $dataMapperMock = $this->createMock(AlbumDataMapper::class);
        $repo = new AlbumRepository($dataMapperMock);

        $dataMapperMock->expects($this->once())
            ->method('findAll')
            ->willReturn(array());

        $repo->findAll();

    }

    public function testFindByNumberOfTrack()
    {
        $dataMapperMock = $this->createMock(AlbumDataMapper::class);
        $albumMock = $this->createMock(Album::class);

        $repo = new AlbumRepository($dataMapperMock);

        $dataMapperMock->expects($this->once())
            ->method('findAll')
            ->willReturn([new Album(), new Album()]);

        $dateWrapper = new DateWrapper();
        $dateWrapper->setDateObject(new \DateTime());
        $this->assertEquals($repo->findWithMoreThenXTracksReleasedBefore(10, $dateWrapper), array());

    }

    public function testFindByNumberOfTrackReturnsThreeAlbums()
    {
        $dataMapperMock = $this->createMock(AlbumDataMapper::class);

        $repo = new AlbumRepository($dataMapperMock);
        $album1 = $this->createAlbumAndAddTracks(15);
        $album2 = $this->createAlbumAndAddTracks(15);
        $album3 = $this->createAlbumAndAddTracks(15);

        $dataMapperMock->expects($this->once())
            ->method('findAll')
            ->willReturn([$album1, $album2, $album3]);
        $dateWrapper = new DateWrapper();
        $dateWrapper->setDateObject(new \DateTime());
        $this->assertEquals($repo->findWithMoreThenXTracksReleasedBefore(10, $dateWrapper), [$album1, $album2, $album3]);
    }

    public function testFindByNumberOfTrackReturnsThreeAlbumsXX()
    {
        $dataMapperMock = $this->createMock(AlbumDataMapper::class);
        $albumMock = $this->createMock(Album::class);

        $repo = new AlbumRepository($dataMapperMock);
        $album1 = $this->createAlbumAndAddTracks(15);
        $album2 = $this->createAlbumAndAddTracks(15);
        $album3 = $this->createAlbumAndAddTracks(5);

        $dataMapperMock->expects($this->once())
            ->method('findAll')
            ->willReturn([$album1, $album2, $album3]);

        $albumMock->expects($this->any())
            ->method('getTracks');
        $dateWrapper = new DateWrapper();
        $dateWrapper->setDateObject(new \DateTime());
        $this->assertEquals($repo->findWithMoreThenXTracksReleasedBefore(10, $dateWrapper), [$album1, $album2]);
    }

    public function testFindByNumberOfTrackReturnsThreeAlbumsXXYY()
    {
        $dataMapperMock = $this->createMock(AlbumDataMapper::class);
        $repo = new AlbumRepository($dataMapperMock);

        $album1 = $this->createAlbumAndAddTracks(20);
        $album2 = $this->createAlbumAndAddTracks(15);
        $album3 = $this->createAlbumAndAddTracks(5);

        $dataMapperMock->expects($this->once())
            ->method('findAll')
            ->willReturn([$album1, $album2, $album3]);
        $dateWrapper = new DateWrapper();
        $dateWrapper->setDateObject(new \DateTime());
        $this->assertEquals($repo->findWithMoreThenXTracksReleasedBefore(16, $dateWrapper), [$album1]);
    }


    public function testGetNumberOfTracksIsCalled()
    {

        $dataMapperMock = $this->createMock(AlbumDataMapper::class);
        $albumMock = $this->createMock(Album::class);
        $repo = new AlbumRepository($dataMapperMock);

        $albumMock2 = $this->createMock(Album::class);
        $albumMock3 = $this->createMock(Album::class);

        $albumMock->expects($this->once())
            ->method('getNumberOfTracks')
            ->willReturn(5);
        $albumMock2->expects($this->once())
            ->method('getNumberOfTracks')
            ->willReturn(5);
        $albumMock3->expects($this->once())
            ->method('getNumberOfTracks');

        $dataMapperMock->expects($this->once())
            ->method('findAll')
            ->willReturn([$albumMock, $albumMock2, $albumMock3]);

        $this->assertEquals($repo->findWithMoreThenXTracksReleasedBefore(16, new DateWrapper()), []);

    }

    private function createAlbumAndAddTracks($numberOfTracks)
    {
        $album = new Album();

        for ($i = 1; $i < $numberOfTracks; $i++) {
            $album->addTrack(new Track());
            $album->setReleaseDate(\DateTime::createFromFormat('j-M-Y', '15-Feb-1991'));
        }

        return $album;
    }
}