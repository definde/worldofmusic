<?php
namespace Dba\WorldOfMusic\Tests\Domain\Model\Service;

use Dba\AwesomeMvc\Core\Application;
use Dba\AwesomeMvc\Mvc\Service\ConfigurationService;
use Dba\AwesomeMvc\Mvc\Service\SanitizeService;
use Dba\AwesomeMvc\Persistence\Storage\XmlStorageAdapter;
use Dba\WorldOfMusic\Domain\Model\Entity\Factory;
use Dba\WorldOfMusic\Domain\Model\Entity\Mapper\AlbumDataMapper;
use Dba\WorldOfMusic\Domain\Model\Entity\Mapper\MusicMozEntityMapper;
use Dba\WorldOfMusic\Domain\Model\Entity\Album;
use Dba\WorldOfMusic\Domain\Model\Repository\AlbumRepository;
use Dba\WorldOfMusic\Domain\Model\Service\AlbumQueryService;
use Dba\WorldOfMusic\Domain\Model\Service\DateAndTimeService;
use Dba\WorldOfMusic\Domain\Model\Service\ReleaseListingService;
use Dba\WorldOfMusic\Domain\Model\Service\XmlService;
use Dba\WorldOfMusic\Lib\DateWrapper;
use Dba\WorldOfMusic\Lib\Xml\SimpleXmlAdapter;
use Dba\WorldOfMusic\Lib\Xml\XmlHandler;
use PHPUnit\Framework\TestCase;

class AlbumQueryServiceTest extends TestCase
{

    protected $rlService;
    protected $queryService;
    protected $xmlService;


    public function setUp()
    {
        $configService = new ConfigurationService();
        $app = new Application();
        $app->setDocumentRoot('/var/www/');
        $configService->setApp($app);

        $mapper = new MusicMozEntityMapper(new Factory($configService), new DateAndTimeService($configService));
        $storage = new XmlStorageAdapter($configService);
        $storage->setXmlFileName('worldofmusic.xml');
        $dataMapper = new AlbumDataMapper($storage, $mapper);
        $albumRepo = new AlbumRepository($dataMapper);

        $xmlAdapter = new SimpleXmlAdapter();
        $xmlHandler = new XmlHandler($xmlAdapter);
        $sanService = new SanitizeService($configService);
        $xmlService = new XmlService($configService, $xmlHandler, $sanService);

        $this->xmlService = $xmlService;

        $this->queryService = new AlbumQueryService($configService, $albumRepo);
    }


    public function testConstructorSetsRepository()
    {
        $albumRepoMock = $this->createMock(AlbumRepository::class);
        $configServiceMock = $this->createMock(ConfigurationService::class);

        $service = new AlbumQueryService($configServiceMock, $albumRepoMock);

        $this->assertInstanceOf(AlbumRepository::class, $service->getRepository());
        $this->assertInstanceOf(ConfigurationService::class, $service->getConfigurationService());
    }


    public function testFindByNumberOfTracksQuery()
    {

        $repoMock = $this->createMock(AlbumRepository::class);
        $this->queryService->setRepository($repoMock);

        $repoMock->expects($this->once())
            ->method('findWithMoreThenXTracksReleasedBefore')
            ->with(22)
            ->willReturn([new Album()]);

        $this->assertInternalType('array', $this->queryService->findWithMoreThenXTracksReleasedBefore(22, new DateWrapper()));
    }


    public function testFindByNumberOfTracksQueryRunsDefaultWith10()
    {

        $repoMock = $this->createMock(AlbumRepository::class);
        $this->queryService->setRepository($repoMock);

        $repoMock->expects($this->once())
            ->method('findWithMoreThenXTracksReleasedBefore')
            ->with(10);

        $this->queryService->findWithMoreThenXTracksReleasedBefore(10, new DateWrapper());
    }


}