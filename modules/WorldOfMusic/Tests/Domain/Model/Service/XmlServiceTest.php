<?php
namespace Dba\WorldOfMusic\Tests\Domain\Model\Service;

use Dba\AwesomeMvc\Core\Application;
use Dba\AwesomeMvc\Mvc\Service\ConfigurationService;
use Dba\AwesomeMvc\Mvc\Service\SanitizeService;
use Dba\AwesomeMvc\Persistence\Storage\XmlStorageAdapter;
use Dba\WorldOfMusic\Domain\Model\Entity\Factory;
use Dba\WorldOfMusic\Domain\Model\Entity\Mapper\AlbumDataMapper;
use Dba\WorldOfMusic\Domain\Model\Entity\Mapper\MusicMozEntityMapper;
use Dba\WorldOfMusic\Domain\Model\Entity\Album;
use Dba\WorldOfMusic\Domain\Model\Repository\AlbumRepository;
use Dba\WorldOfMusic\Domain\Model\Service\AlbumQueryService;
use Dba\WorldOfMusic\Domain\Model\Service\DateAndTimeService;
use Dba\WorldOfMusic\Domain\Model\Service\ReleaseListingService;
use Dba\WorldOfMusic\Domain\Model\Service\XmlService;
use Dba\WorldOfMusic\Lib\Xml\SimpleXmlAdapter;
use Dba\WorldOfMusic\Lib\Xml\XmlHandler;
use PHPUnit\Framework\TestCase;

class XmlServiceTest extends TestCase
{

    protected $rlService;
    protected $queryService;
    protected $xmlService;


    public function setUp()
    {
        $configService = new ConfigurationService();
        $app = new Application();
        $app->setDocumentRoot('/var/www/');
        $configService->setApp($app);
        $factory = new Factory($configService);
        $mapper = new MusicMozEntityMapper($factory, new DateAndTimeService($configService));
        $storage = new XmlStorageAdapter($configService);
        $storage->setXmlFileName('worldofmusic.xml');
        $dataMapper = new AlbumDataMapper($storage, $mapper);
        $albumRepo = new AlbumRepository($dataMapper);

        $xmlAdapter = new SimpleXmlAdapter();
        $xmlHandler = new XmlHandler($xmlAdapter);
        $sanService = new SanitizeService($configService);
        $xmlService = new XmlService($configService, $xmlHandler, $sanService);

        $this->xmlService = $xmlService;

        $this->queryService = new AlbumQueryService($configService, $albumRepo);

        $rlService = new ReleaseListingService($configService, $xmlService, $this->queryService);
        $this->rlService = $rlService;
    }

    public function testConstructor(){
        $configMock = $this->createMock(ConfigurationService::class);
        $xmlHandlerMock = $this->createMock(XmlHandler::class);
        $saniMock = $this->createMock(SanitizeService::class);

        $xmlService = new XmlService($configMock, $xmlHandlerMock, $saniMock);

        $this->assertInstanceOf(ConfigurationService::class, $xmlService->getConfigurationService());
        $this->assertInstanceOf(XmlHandler::class, $xmlService->getXmlHandler());
        $this->assertInstanceOf(SanitizeService::class, $xmlService->getSanitizeService());
    }





}