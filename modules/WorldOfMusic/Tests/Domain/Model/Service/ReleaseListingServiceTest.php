<?php
namespace Dba\WorldOfMusic\Tests\Domain\Model\Service;

use Dba\AwesomeMvc\Core\Application;
use Dba\AwesomeMvc\Mvc\Service\ConfigurationService;
use Dba\AwesomeMvc\Mvc\Service\SanitizeService;
use Dba\AwesomeMvc\Persistence\Storage\XmlStorageAdapter;
use Dba\WorldOfMusic\Domain\Model\Entity\Factory;
use Dba\WorldOfMusic\Domain\Model\Entity\Mapper\AlbumDataMapper;
use Dba\WorldOfMusic\Domain\Model\Entity\Mapper\MusicMozEntityMapper;
use Dba\WorldOfMusic\Domain\Model\Entity\Album;
use Dba\WorldOfMusic\Domain\Model\Repository\AlbumRepository;
use Dba\WorldOfMusic\Domain\Model\Service\AlbumQueryService;
use Dba\WorldOfMusic\Domain\Model\Service\DateAndTimeService;
use Dba\WorldOfMusic\Domain\Model\Service\ReleaseListingService;
use Dba\WorldOfMusic\Domain\Model\Service\XmlService;
use Dba\WorldOfMusic\Lib\DateWrapper;
use Dba\WorldOfMusic\Lib\Xml\SimpleXmlAdapter;
use Dba\WorldOfMusic\Lib\Xml\XmlHandler;
use PHPUnit\Framework\TestCase;

class ReleaseListingServiceTest extends TestCase
{

    protected $rlService;
    protected $queryService;
    protected $xmlService;


    public function setUp()
    {
        $configService = new ConfigurationService();
        $app = new Application();
        $app->setDocumentRoot('/var/www/');
        $configService->setApp($app);
        $factory = new Factory($configService);
        $mapper = new MusicMozEntityMapper($factory, new DateAndTimeService($configService));
        $storage = new XmlStorageAdapter($configService);
        $storage->setXmlFileName('worldofmusic.xml');
        $dataMapper = new AlbumDataMapper($storage, $mapper);
        $albumRepo = new AlbumRepository($dataMapper);

        $xmlAdapter = new SimpleXmlAdapter();
        $xmlHandler = new XmlHandler($xmlAdapter);
        $sanService = new SanitizeService($configService);
        $xmlService = new XmlService($configService, $xmlHandler, $sanService);

        $this->xmlService = $xmlService;

        $this->queryService = new AlbumQueryService($configService, $albumRepo);

        $rlService = new ReleaseListingService($configService, $xmlService, $this->queryService);
        $this->rlService = $rlService;
    }


    public function testSetGetQueryService()
    {
        $this->rlService->setQueryService($this->queryService);
        $this->assertInstanceOf(AlbumQueryService::class, $this->rlService->getQueryService());
    }


    public function testSetGetXmlService()
    {
        $this->rlService->setXmlService($this->xmlService);
        $this->assertInstanceOf(XmlService::class, $this->rlService->getXmlService());
    }

    public function testCreateXmlListOfAlbumsByNumberOfTracks()
    {
        $albums = [new Album(), new Album()];
        $xmlMock = $this->createMock(XmlService::class);

        $albumMock = $this->createMock(AlbumQueryService::class);
        $this->rlService->setQueryService($albumMock);
        $this->rlService->setXmlService($xmlMock);

        $xmlMock->expects($this->once())
            ->method('createAlbumShortlistAsXml');

        $albumMock->expects($this->once())
            ->method('findWithMoreThenXTracksReleasedBefore')
            ->willReturn($albums);

        $this->rlService->createXmlWithMoreThanXTracksAndBeforeReleaseDate(10, new DateWrapper());

    }

    public function testCreateAlbumShortlistAsXml(){

        $albums = $this->albumProvider();
        $element = new \SimpleXMLElement('<data></data>');

        $handler = $this->getMockBuilder(XmlHandler::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->xmlService->setXmlHandler($handler);

        $handler->expects($this->exactly(9))
            ->method('addChild')
            ->willReturn($element);

        $handler->expects($this->once())
            ->method('asXML')
            ->willReturn(true);
        $filename = "soemtnhin";

        $this->xmlService->createAlbumShortlistAsXml($albums, $filename);
    }


    public function albumProvider(){
        $albums = array();
        for($i = 1; $i < 10; $i++){
            $album = new Album();
            $album->setReleaseDate(new \DateTime());
            $albums[] = $album;
        }
        return $albums;
    }


}