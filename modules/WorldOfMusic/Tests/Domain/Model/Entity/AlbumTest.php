<?php
namespace Dba\WorldOfMusic\Tests\Domain\Model\Entity;

use Dba\WorldOfMusic\Domain\Model\Entity\AlbumFormat;
use Dba\WorldOfMusic\Domain\Model\Entity\Track;
use PHPUnit\Framework\TestCase;
use Dba\WorldOfMusic\Domain\Model\Entity\Album;

class AlbumTest extends TestCase
{

    /**
     *
     */
    public function testSetGet(){
        $properties = ['id','title', 'name', 'genre', 'releaseDate', 'label', 'formats', 'tracks'];
        $album = new Album();
        foreach($properties as $property ){
            $testValue = "qweqwe";
            $getter = "get" . ucfirst($property);
            $setter = "set" . ucfirst($property);
            $album->$setter($testValue);
            $this->assertEquals($testValue, $album->$getter());
        }
    }

    public function testAddFormat(){
        $album = new Album();
        $format = new AlbumFormat();
        $format2 = new AlbumFormat();

        $album->addFormat($format);
        $formats = $album->getFormats();
        $this->assertInstanceOf(AlbumFormat::class, $formats[0]);


        $album->addFormat($format2);
        $formats = $album->getFormats();
        $this->assertEquals(2, count($formats));

    }

    /**
     * @expectedException \PHPUnit_Framework_Error
     */
    public function testAddFormatException(){
        $album = new Album();
        $album->addFormat('somethingWrong');
    }


    public function testAddTrack(){
        $album = new Album();
        $track = new Track();
        $track2 = new Track();

        $album->addTrack($track);
        $tracks = $album->getTracks();
        $this->assertInstanceOf(Track::class, $tracks[0]);


        $album->addTrack($track2);
        $tracks = $album->getTracks();
        $this->assertEquals(2, count($tracks));

    }

    /**
     * @expectedException \PHPUnit_Framework_Error
     */
    public function testAddTrackException(){
        $album = new Album();
        $album->addTrack('somethingWrong');
    }
}
?>