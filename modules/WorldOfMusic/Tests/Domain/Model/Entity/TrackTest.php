<?php
namespace Dba\WorldOfMusic\Tests\Domain\Model\Entity;

use Dba\WorldOfMusic\Domain\Model\Entity\AlbumFormat;
use Dba\WorldOfMusic\Domain\Model\Entity\Track;
use PHPUnit\Framework\TestCase;
use Dba\WorldOfMusic\Domain\Model\Entity\Album;

class TrackTest extends TestCase
{

    /**
     *
     */
    public function testSetGet(){
        $properties = ['title','id'];
        $track = new Track();
        foreach($properties as $property ){
            $testValue = "qweqwe";
            $getter = "get" . ucfirst($property);
            $setter = "set" . ucfirst($property);
            $track->$setter($testValue);
            $this->assertEquals($testValue, $track->$getter());
        }
    }

}
?>