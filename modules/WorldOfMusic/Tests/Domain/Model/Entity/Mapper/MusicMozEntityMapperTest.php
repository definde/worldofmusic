<?php

namespace Dba\WorldOfMusic\Tests\Domain\Model\Entity\Mapper;

use Dba\AwesomeMvc\Persistence\Storage\XmlStorageAdapter;
use Dba\WorldOfMusic\Domain\Model\Entity\Album;
use Dba\WorldOfMusic\Domain\Model\Entity\Factory;
use Dba\WorldOfMusic\Domain\Model\Entity\Mapper\MusicMozEntityMapper;
use Dba\WorldOfMusic\Domain\Model\Service\DateAndTimeService;
use PHPUnit\Framework\TestCase;

class MusicMozEntityMapperTest extends TestCase {


    public function testCreateEntityFromRow(){


        $factoryMock = $this->createMock(Factory::class);

        $entityMock = $this->createMock(Album::class);
        $row = new \stdClass();

        $row->title = new \SimpleXMLElement('<title>something</title>');
        $row->name = new \SimpleXMLElement('<name>something</name>');
        $row->genre = new \SimpleXMLElement('<name>something</name>');
        $row->releasedate = new \SimpleXMLElement('<name>something</name>');
        $row->label = new \SimpleXMLElement('<name>something</name>');

        $row->tracklisting->track[] = new \SimpleXMLElement('<name>something</name>');
        $row->tracklisting->track[] = new \SimpleXMLElement('<name>something</name>');

        $row->formats = new \SimpleXMLElement('<formats>something,anotherone</formats>');

        $factoryMock->expects($this->once())
            ->method('createEntity')
            ->willReturn($entityMock);

        $entityMock->expects($this->once())
            ->method('setTitle');

        $mapper = new MusicMozEntityMapper($factoryMock, $this->createMock(DateAndTimeService::class) );
        $mapper->setEntityFactory($factoryMock);
        $mapper->createEntityFromRow($row);
    }



}