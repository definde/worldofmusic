<?php

namespace Dba\WorldOfMusic\Tests\Domain\Model\Entity\Mapper;


use Dba\AwesomeMvc\Persistence\Storage\XmlStorageAdapter;
use Dba\WorldOfMusic\Domain\Model\Entity\Mapper\AlbumDataMapper;
use Dba\WorldOfMusic\Domain\Model\Entity\Mapper\MusicMozEntityMapper;
use Dba\WorldOfMusic\Lib\Xml\SimpleXmlAdapter;
use PHPUnit\Framework\TestCase;

class AlbumDataMapperTest extends TestCase
{

    public function testConstructor()
    {
        $xmlAdMock = $this->createMock(XmlStorageAdapter::class);
        $mumozMock = $this->createMock(MusicMozEntityMapper::class);

        $xd = new AlbumDataMapper($xmlAdMock, $mumozMock);

        $this->assertInstanceOf(XmlStorageAdapter::class, $xd->getStorageAdapter());
        $this->assertInstanceOf(MusicMozEntityMapper::class, $xd->getEntityMapper());
    }

    public function testSetter()
    {
        $xmlAdMock = $this->createMock(XmlStorageAdapter::class);
        $mumozMock = $this->createMock(MusicMozEntityMapper::class);

        $xd = new AlbumDataMapper($xmlAdMock, $mumozMock);

        $anotherxmlAdMock = $this->createMock(XmlStorageAdapter::class);
        $anothermumozMock = $this->createMock(MusicMozEntityMapper::class);

        $xd->setEntityMapper($anothermumozMock);
        $xd->setStorageAdapter($anotherxmlAdMock);

        $this->assertEquals($anotherxmlAdMock, $xd->getStorageAdapter());
        $this->assertEquals($anothermumozMock, $xd->getEntityMapper());
    }
}