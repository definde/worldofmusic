<?php
namespace Dba\WorldOfMusic\Tests\Domain\Model\Entity;

use Dba\WorldOfMusic\Domain\Model\Entity\AlbumFormat;
use Dba\WorldOfMusic\Domain\Model\Entity\Track;
use PHPUnit\Framework\TestCase;
use Dba\WorldOfMusic\Domain\Model\Entity\Album;

class AlbumFormatTest extends TestCase
{

    /**
     *
     */
    public function testSetGet(){
        $properties = ['format','id'];
        $format = new AlbumFormat();
        foreach($properties as $property ){
            $testValue = "qweqwe";
            $getter = "get" . ucfirst($property);
            $setter = "set" . ucfirst($property);
            $format->$setter($testValue);
            $this->assertEquals($testValue, $format->$getter());
        }
    }

}
?>