<?php

namespace Dba\WorldOfMusic\Tests\Controller;


use Dba\AwesomeMvc\Core\Application;
use Dba\AwesomeMvc\Mvc\Request\CliRequest;
use Dba\AwesomeMvc\Mvc\Request\RequestInterface;
use Dba\WorldOfMusic\Controller\AlbumController;
use Dba\WorldOfMusic\Domain\Model\Service\DateAndTimeService;
use Dba\WorldOfMusic\Domain\Model\Service\ReleaseListingService;
use Dba\WorldOfMusic\Lib\DateWrapper;
use PHPUnit\Framework\TestCase;

class AlbumControllerTest extends TestCase {

    public function testConstruction(){
        $requestMock = $this->createMock(RequestInterface::class);
        $appMock = $this->createMock(Application::class);
        new AlbumController($requestMock, $appMock);
    }


    public function testListAlbumsByNumberOfTracksAction(){

        $requestMock = $this->createMock(RequestInterface::class);
        $appMock = $this->createMock(Application::class);
        $rlServMock = $this->createMock(ReleaseListingService::class);
        $dateWr = $this->getMockBuilder(DateWrapper::class);
        $dateTimeSer = $this->createMock(DateAndTimeService::class);
        $appMock->expects($this->exactly(2))
            ->method('get')
            ->willReturnOnConsecutiveCalls($rlServMock,$dateTimeSer);


        $requestMock->expects($this->exactly(2))
            ->method('getParam')
            ->withConsecutive(
                ['moreTracksThan'],
                ['releaseBefore']
            )
            ;

        $rlServMock->expects($this->once())
            ->method('createXmlWithMoreThanXTracksAndBeforeReleaseDate')
            ->with(10);

        $controller = new AlbumController($requestMock, $appMock);
        $controller->listByMoreThenXTracksAndReleasedBeforeDateAction();
    }
}