<?php


namespace Dba\WorldOfMusic\Tests\Lib;


use Dba\WorldOfMusic\Lib\Xml\SimpleXmlAdapter;
use PHPUnit\Framework\TestCase;

class SimpleXmlAdapterTest extends TestCase {

    public function testConstructor(){
        $simpleXml = new SimpleXmlAdapter();

        $this->assertInstanceOf(\SimpleXMLElement::class, $simpleXml->getSimpleXml());
    }


    public function testAddChild(){
        $simpleXml = new SimpleXmlAdapter();

        $xmlMock = $this
            ->getMockBuilder('Traversable')
            ->setMethods(['addChild','current','next','key', 'valid','rewind'])
            ->getMock();
        $simpleXml->setSimpleXml($xmlMock);

        $xmlMock->expects($this->once())
            ->method('addChild')
            ->with('someName');


        $simpleXml->addChild('someName');
        unset($xmlMock);
    }


    public function testAsXml(){
        $simpleXml = new SimpleXmlAdapter();

        $xmlMock22 = $this
            ->getMockBuilder('Traversable')
            ->setMethods(['asXml','current','next','key', 'valid','rewind'])
            ->getMock();
        $simpleXml->setSimpleXml($xmlMock22);

        $xmlMock22->expects($this->once())
            ->method('asXml')
            ->with('someName');


        $simpleXml->asXml('someName');
    }

}