<?php


namespace Dba\WorldOfMusic\Tests\Lib;


use Dba\WorldOfMusic\Lib\Xml\SimpleXmlAdapter;
use Dba\WorldOfMusic\Lib\Xml\XmlHandler;
use PHPUnit\Framework\TestCase;

class XmlHandlerTest extends TestCase {

    public function testSomething(){
        $this->assertEquals(true, true);
    }


    public function testConstructor(){
        $adapterMocj = $this->createMock(SimpleXmlAdapter::class);


        $handler = new XmlHandler($adapterMocj);
        $this->assertEquals($adapterMocj, $handler->getAdapter());
    }

    public function testAddChild(){
        $adapterMocj = $this->createMock(SimpleXmlAdapter::class);

        $adapterMocj->expects($this->once())
            ->method('addChild')
            ->with('ss');


        $handler = new XmlHandler($adapterMocj);
        $handler->addChild('ss');
    }

    public function testAsXml(){
        $adapterMocj = $this->createMock(SimpleXmlAdapter::class);

        $adapterMocj->expects($this->once())
            ->method('asXml')
            ->with('ss');


        $handler = new XmlHandler($adapterMocj);
        $handler->asXml('ss');
    }

}