#!/usr/bin/env php
<?php
require_once( __DIR__ . '/../vendor/autoload.php');

$request = new Dba\AwesomeMvc\Mvc\Request\CliRequest($_SERVER);
$app = new \Dba\AwesomeMvc\Core\Application();
$app->setDocumentRoot( __DIR__ . '/../') ;
$app->run($request);