# Developers Recruiting Test
## World of music
### Introduction

Hey guys, 

since this is a development task I decided not to take an existing MVC framework and write a little tiny and simple one by myself. There is still  much missing but at least you can dispatch a command from the commandline. :-)
You can find the sources of the framework in the zip file (vendor/dba/awesomemvc/) or in the correspondig git repository, which can be found here: https://gitlab.com/definde/AwesomeMVC
 
The worldofmusic project, which contains the business logic part, can be found in the zip under "modules/WorldOfMusic" or in the git right here: https://gitlab.com/definde/worldofmusic

I made a folder for the unit test coverage in the ZIP-file as well.

### Installation:

a) You can just use the zip extract it to somewhere where a php interpreter is availabe and run via command line. 

b) You can use composer to install the package. Just run the following steps:
1. clone the git from https://gitlab.com/definde/worldofmusic.git 
2. Run composer install which should install all dependencies.
3. After that you should be able to run from the command line.

### Running from the command line
You can call the cli command very easily. It basically consists of three different parts.
1. First part is of course the commandline dispatcher script (php cli/aws_cli.php)
2. Second part is the combination of Module:Controller:Action you would like to call (WorldOfMusic:Album:listByMoreThenXTracksAndReleasedBeforeDate)
3. Last part are OPTIONAL parameters. The command line parser will recognize key -> value pairs
you just have to sepearte them by a space. (ie. moreTracksThan 30 releaseDate 01/01/1995)

From the folder you unzipped the package or installed it with composer you call
the dispatcher with:

**php cli/aws_cli.php WorldOfMusic:Album:listByMoreThenXTracksAndReleasedBeforeDate**

If you'ld like to mess around with some params you can try to use something like that:

**php cli/aws_cli.php WorldOfMusic:Album:listByMoreThenXTracksAndReleasedBeforeDate moreTracksThan 30 releaseDate 01/01/1995**

### Upcoming requirements
#### 1. The MusicMoz Data is not available anymore -but there is a new fancy online service with almost the same data but a different xmlstructure
   
It should be quite easy to handle this new requirement. First of all the business layer is completely decoupled from the persistence layer
so we do not have to change anything within our businesslogic.

But we have to change some things on the persistence layer which should not be to hard either. Since
we are currently using a xml file as our storage I wrote a XmlStorageAdapter to access that storage.
We can now easily write a new storage, for example a WebserviceStorageAdapter and inject it to the data-mapper. 

Because the xml structure is going to change we will also have to write/change the datamapper and entitymapper which creates the entities.
            
#### 2. "WorldOfMusic"wants to have another XML written, that lists all Releases with at least 2 Compact Discs
   

I think this should be already possible, but I'm not sure what you exactly mean with 2 Compact Discs. Did you mean 2 tracks ?
Well if you meant 2 tracks you could use the cli like that:

**php cli/aws_cli.php WorldOfMusic:Album:listByMoreThenXTracksAndReleasedBeforeDate moreTracksThan 1 releaseBefore 01/01/2500**

Keep in mind that you have to use 1 as parameter to get at least 2. Release Date should be far in the future to get all releases. (Well this is a bit hacky...)
   
   
### Time Tracking

It was roughly
 - a day for the framework
 - a day for the businesslogicpart and
 - a day for the unit testing stuff

So three days at all (with 8 working hours each).



       




  

